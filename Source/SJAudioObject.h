//
//  SJAudioObject.h
//  SJLiveVideo
//
//  Created by king on 16/6/19.
//  Copyright © 2016年 king. All rights reserved.
//

#import "Common.h"
#import <Foundation/Foundation.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale//swscale.h>

@interface SJAudioObject : NSObject

- (instancetype)initWithVideo:(NSString *)moviePath;
- (NSTimeInterval)duration;
- (void)seekTime:(NSTimeInterval)seconds;
- (AVPacket*)readPacket;
- (NSInteger)decode;
@end
