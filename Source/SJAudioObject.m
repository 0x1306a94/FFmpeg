//
//  SJAudioObject.m
//  SJLiveVideo
//
//  Created by king on 16/6/19.
//  Copyright © 2016年 king. All rights reserved.
//

#import "SJAudioObject.h"

@implementation SJAudioObject
{
    AVFormatContext             *SJAudioFormatCtx;
    AVCodecContext              *SJAudioCodecCtx;
    AVStream                    *audioStream;
    AVCodec                     *SJAudioCodec;
    AVPacket                    audioPacket;
    AVPacket                    currentPacket;
    AVFrame                     *decodedFrame;
    BOOL                        inBuffer;
    int                         audioStreamIndex;
    int                         decodedDataSize;
    int16_t                     *audioBuffer;
    int                         audioBufferSize;
}

- (instancetype)initWithVideo:(NSString *)moviePath {
    
    if (!(self=[super init])) return nil;
        
        av_register_all();
        avformat_network_init();
        av_init_packet(&audioPacket);
        audioStreamIndex = -1;
        audioBufferSize = 192000;
        audioBuffer = av_malloc(audioBufferSize);
        inBuffer = NO;
        // 打开文件
        if (avformat_open_input(&SJAudioFormatCtx, [moviePath UTF8String], NULL, NULL) != 0) {
            SJLog(@"文件打开失败")
            goto initError;
        }
        // 检查数据流
        if (avformat_find_stream_info(SJAudioFormatCtx, NULL) < 0) {
            SJLog(@"检查数据流失败")
            goto initError;
        }
        // 根据数据流,查找第一个音频流
        audioStreamIndex   = -1;
        if ((audioStreamIndex = av_find_best_stream(SJAudioFormatCtx, AVMEDIA_TYPE_AUDIO, -1, -1, &SJAudioCodec, 0)) < 0) {
            SJLog(@"没有找到音频流");
            goto initError;
        }
        audioStream        = SJAudioFormatCtx->streams[audioStreamIndex];
        // 获取音频流的编解码上下文指针
        SJAudioCodecCtx    = audioStream->codec;
        // 查找音频流解码器
        if ((SJAudioCodec = avcodec_find_decoder(SJAudioCodecCtx->codec_id)) == NULL) {
            SJLog(@"没有找到音频流解码器")
            goto initError;
        }
        // 开启解码器
        if (avcodec_open2(SJAudioCodecCtx, SJAudioCodec, NULL) < 0) {
            SJLog(@"音频解码器打开失败")
            goto initError;
        }
        SJLog(@" bit_rate = %lld \r\n", SJAudioCodecCtx->bit_rate)
        SJLog(@" sample_rate = %d \r\n", SJAudioCodecCtx->sample_rate)
        SJLog(@" channels = %d \r\n", SJAudioCodecCtx->channels)
        SJLog(@" code_name = %s \r\n", SJAudioCodecCtx->codec->name)
        SJLog(@" block_align = %d\n", SJAudioCodecCtx->block_align)
        decodedFrame = av_frame_alloc();
        return self;
initError:
        return nil;
}
- (NSTimeInterval)duration {
    return SJAudioFormatCtx == NULL ?
    0.0f : (NSTimeInterval)SJAudioFormatCtx->duration / AV_TIME_BASE;
}

- (void)seekTime:(NSTimeInterval)seconds {
    inBuffer = NO;
    av_free_packet(&audioPacket);
    currentPacket = audioPacket;
    av_seek_frame(SJAudioFormatCtx, -1, seconds * AV_TIME_BASE, 0);
}
- (AVPacket *)readPacket {
    
    if (currentPacket.size > 0 || inBuffer) {
        return &currentPacket;
    }
    av_free_packet(&audioPacket);
    
    for (; ; ) {
        NSInteger ret = av_read_frame(SJAudioFormatCtx, &audioPacket);
        if (ret == AVERROR(EAGAIN)) {
            continue;
        } else if (ret < 0) {
            return NULL;
        }
        
        if (audioPacket.stream_index != audioStreamIndex) {
            av_free_packet(&audioPacket);
            continue;
        }
        if (audioPacket.dts != AV_NOPTS_VALUE) {
            audioPacket.dts += av_rescale_q(0, AV_TIME_BASE_Q, audioStream->time_base);
        }
        if (audioPacket.pts != AV_NOPTS_VALUE) {
            audioPacket.pts += av_rescale_q(0, AV_TIME_BASE_Q, audioStream->time_base);
        }
        break;
    }
    currentPacket = audioPacket;
    return &currentPacket;
}
- (NSInteger)decode {
    if (inBuffer) {
        return decodedDataSize;
    }
    decodedDataSize = 0;
    AVPacket *packet = [self readPacket];
    while (packet && packet->size > 0) {
        if (audioBufferSize < FFMAX(packet->size * sizeof(*audioBuffer), 192000)) {
            audioBufferSize = FFMAX(packet->size * sizeof(*audioBuffer), 192000);
            av_free(audioBuffer);
            audioBuffer = av_malloc(audioBufferSize);
        }
        decodedDataSize = audioBufferSize;
        int len = avcodec_decode_audio4(SJAudioCodecCtx, decodedFrame, &decodedDataSize, packet);
        if (len < 0) {
            SJLog(@"Could not decode audio packet.")
            return 0;
        }
        packet->data += len;
        packet->size -= len;
        if (decodedDataSize <= 0) {
            SJLog(@"Decoding was completed.");
            packet = NULL;
            return 0;
        }
        
        inBuffer = YES;
        break;
    }
    return decodedDataSize;
}

@end
