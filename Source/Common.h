//
//  Common.h
//  SJLiveVideo
//
//  Created by king on 16/6/19.
//  Copyright © 2016年 king. All rights reserved.
//

#ifndef Common_h
#define Common_h

#ifdef DEBUG
#define SJLog(fmt, ...)     NSLog((fmt), ##__VA_ARGS__);
#define SJLogFunc           NSLog(@"%s(Line: %d)", __func__, __LINE__);
#else
#define SJLog(...);
#define SJLogFunc;
#endif

#endif /* Common_h */
